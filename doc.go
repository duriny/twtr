// twtr is a modular, hackable, and decentralised, microblogging client for the
// twtxt protocol.
//
//     go install duriny.envs.sh/twtr@latest
//
// NAME
//
// twtr - a decentralised microblogging client.
//
// SYNOPSIS
//
// The general syntax for twtr is:
//
//     twtr COMMAND [OPTIONS] [ARGS ...]
//
// Where the command specifies the action to preform, with optional flags, as
// well as any arguments to that command. For more information on each command,
// see the relevant entry in the COMMANDS section, or see the help message for
// each command:
//
//     twtr COMMAND --help
//
// COMMANDS
//
// The following are the commands twtr provides.
//
// INIT COMMAND
//
// Initialise twtr locally.
//
//     twtr init [-h|--help] [--file FILE]
//
// The init command initialises a new twtxt.txt file for the user, the twtxt
// file location defaults to $XDG_DATA_HOME/twtr/twtxt.txt, but this may be
// overridden with the --file flag.
//
// POST COMMAND
//
// Post a new tweet.
//
//     twtr post [-h|--help] [--file FILE] TWEET...
//
// The post command writes a new post to the specified twtxt.txt file and runs
// the pre/post tweet hooks.
//
// COPYRIGHT
//
// All rites reversed, use, distribute, and modify freely.
//
// AUTHOR
//
// ~duriny <duriny@envs.net>
//
// BUGS
//
// Probably. Let me know if you find any.
//
// SEE ALSO
//
// There are a number of other twtxt clients still under active development.
//
//     jenny(1)    - https://www.uninformativ.de/git/jenny/file/README.html
//     twet(1)     - https://github.com/jdtron/twet
//     twtxt(1)    - https://github.com/buckket/twtxt (unmaintained)
//     Yarn.Social - https://yarn.social
//
// Or, you can use the duriny.envs.sh/twtr/cmd package to build your own version
// of twtr. The cmd package provides a modular framework to mix and match twtr
// components and even to add your own.
//
package main // import duriny.envs.sh/twtr
