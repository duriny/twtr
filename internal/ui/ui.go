// Package ui is an internal package of twtr/cmd.
//
// It provides helpers interact with the user.
package ui

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// GetInput from os.Stdin, takes a default value, if the user doesn't provide
// any input, the default value is returned.
func GetInput(prompt string, def string) (val string, err error) {
	// show the user a prompt
	if def == "" {
		fmt.Printf("%s: ", prompt)
	} else {
		fmt.Printf("%s [%s]: ", prompt, def)
	}

	// read the user's response, checking for any errors
	if val, err = bufio.NewReader(os.Stdin).ReadString('\n'); err != nil {
		return
	}

	// trim any extra whitespace from the user's input
	val = strings.TrimSpace(val)

	// fall back to the default if the user hasn't specified anything
	if val == "" {
		val = def
	}

	return
}

// GetInputBool from os.Stdin, takes a default and parses the user's input
// as a boolean, or returns the default if the user doesn't provide any input.
func GetInputBool(prompt string, def bool) (val bool, err error) {
	// show the user a prompt
	if def {
		fmt.Printf("%s [Y/n]: ", prompt)
	} else {
		fmt.Printf("%s [y/N]: ", prompt)
	}

	// read the user's response, checking for any errors
	var input string
	if input, err = bufio.NewReader(os.Stdin).ReadString('\n'); err != nil {
		return
	}

	// trim any extra whitespace from the user's input
	input = strings.TrimSpace(input)

	// fall back to the default if the user hasn't specified a valid bool
	switch strings.ToLower(input) {
	case "y", "yes":
		val = true

	case "n", "no":
		val = false

	default:
		val = def
	}

	return
}
