// Package files is an internal package of twtr/cmd.
//
// It provides helpers to access the file system.
package files

import (
	"errors"
	"io"
	"os"
	"os/exec"
	"path"
	"sync"

	"github.com/spf13/afero"
)

// fs is the file system backend, it is guarded by a mutex, swapping the backend
// must be atomic.
var fs = struct {
	afero.Afero
	sync.Mutex
}{Afero: afero.Afero{afero.NewOsFs()}}

// mkdir ensures that the base directory of the given fully qualified file
// exists.
func mkdir(file string) error {
	// get the directory this file is in
	dir := path.Dir(file)

	// make sure that the directory of this file exists
	if err := fs.MkdirAll(dir, 0755); err != nil && !os.IsExist(err) {
		return err
	}

	return nil
}

// Open file with read/write permissions, useful for opening a twtxt.txt file to
// write to.
func Open(file string) (io.ReadWriteCloser, error) {
	if err := mkdir(file); err != nil {
		return nil, err
	}

	return fs.OpenFile(file, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
}

// Create file with read/write permissions, useful for creating a new twtxt.txt
// file (or overwriting) to write to.
func Create(file string) (io.ReadWriteCloser, error) {
	if err := mkdir(file); err != nil {
		return nil, err
	}

	return fs.Create(file)
}

// Exists reports if the given file exists, or any error that it encounters
// while checking.
func Exists(file string) (bool, error) {
	return fs.Exists(file)
}

// Edit a file in the system EDITOR, uses VISUAL over EDITOR if set.
//
// Equivalent to:
//
//    $ sh -c "${VISUAL:-$EDITOR} <file>"
//
// Returns any errors that occur.
func Edit(file string) error {
	EDITOR := os.Getenv("EDITOR")

	if visual := os.Getenv("VISUAL"); visual != "" {
		EDITOR = visual
	}

	if EDITOR == "" {
		return errors.New("EDITOR not set")
	}

	cmd := exec.Command(EDITOR, file)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	return cmd.Run()
}
