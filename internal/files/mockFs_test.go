package files

import (
	"os"
	"testing"
	"time"

	"github.com/spf13/afero"
)

// ensure mockFs implements the afero.Fs interface.
var _ afero.Fs = mockFs{}

// mockFs is a mockable implementation of the afero.Fs interface.
type mockFs struct {
	t         *testing.T
	create    func(string) (afero.File, error)
	mkdir     func(string, os.FileMode) error
	mkdirAll  func(string, os.FileMode) error
	open      func(string) (afero.File, error)
	openFile  func(string, int, os.FileMode) (afero.File, error)
	remove    func(string) error
	removeAll func(string) error
	rename    func(string, string) error
	stat      func(string) (os.FileInfo, error)
	chmod     func(string, os.FileMode) error
	chown     func(string, int, int) error
	chtimes   func(string, time.Time, time.Time) error
}

func (mock mockFs) Name() string {
	return mock.t.Name() + "/MockFS"
}

func (mock mockFs) Create(name string) (file afero.File, err error) {
	if mock.create != nil {
		file, err = mock.create(name)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Create(%q)", name)
	}

	return
}

func (mock mockFs) Mkdir(name string, perm os.FileMode) (err error) {
	if mock.mkdir != nil {
		err = mock.mkdir(name, perm)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Mkdir(%q, %#v)", name, perm)
	}

	return
}

func (mock mockFs) MkdirAll(path string, perm os.FileMode) (err error) {
	if mock.mkdirAll != nil {
		err = mock.mkdirAll(path, perm)
	} else {
		mock.t.Errorf("unexpected call to mockFs.MkdirAll(%q, %#o)", path, perm)
	}

	return
}

func (mock mockFs) Open(name string) (file afero.File, err error) {
	if mock.open != nil {
		file, err = mock.open(name)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Open(%q)", name)
	}

	return
}

func (mock mockFs) OpenFile(name string, flag int, perm os.FileMode) (file afero.File, err error) {
	if mock.openFile != nil {
		file, err = mock.openFile(name, flag, perm)
	} else {
		mock.t.Errorf("unexpected call to mockFs.OpenFile(%q, %#b, %#o)", name, flag, perm)
	}

	return
}

func (mock mockFs) Remove(name string) (err error) {
	if mock.remove != nil {
		err = mock.remove(name)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Remove(%q)", name)
	}

	return
}

func (mock mockFs) RemoveAll(path string) (err error) {
	if mock.removeAll != nil {
		err = mock.removeAll(path)
	} else {
		mock.t.Errorf("unexpected call to mockFs.RemoveAll(%q)", path)
	}

	return
}

func (mock mockFs) Rename(oldname string, newname string) (err error) {
	if mock.rename != nil {
		err = mock.rename(oldname, newname)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Rename(%q, %q)", oldname, newname)
	}

	return
}

func (mock mockFs) Stat(name string) (file os.FileInfo, err error) {
	if mock.stat != nil {
		file, err = mock.stat(name)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Stat(%q)", name)
	}

	return
}

func (mock mockFs) Chmod(name string, mode os.FileMode) (err error) {
	if mock.chmod != nil {
		err = mock.chmod(name, mode)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Chmod(%q, %#o)", name, mode)
	}

	return
}

func (mock mockFs) Chown(name string, uid int, gid int) (err error) {
	if mock.chown != nil {
		err = mock.chown(name, uid, gid)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Chown(%q, %#o, %#o)", name, uid, gid)
	}

	return
}

func (mock mockFs) Chtimes(name string, atime time.Time, mtime time.Time) (err error) {
	if mock.chtimes != nil {
		err = mock.chtimes(name, atime, mtime)
	} else {
		mock.t.Errorf("unexpected call to mockFs.Chtimes(%q, %#v, %#v)", name, atime, mtime)
	}

	return
}

// mockAfero is a helper that mocks out the current afero backend file system
// and returns a rest function that can be used to restore the original system
// after the test is done with the mock. This design allows for the mock and
// rest action to be done as a single defer statement, as only the last function
// call in the chain is deferred, the mock setup is done instantaneously, while
// the reset itself is deferred, also since deferred functions us their
// parameters at defer time, not at call time, the rest will. safely use the
// original.
func mockAfero(mock afero.Fs) (reset func(original afero.Fs)) {
	fs.Lock()

	fs.Afero.Fs = mock

	return func(original afero.Fs) {
		fs.Afero.Fs = original
		fs.Unlock()
	}
}
