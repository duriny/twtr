package files

import (
	"errors"
	"os"
	"testing"

	"github.com/spf13/afero"
)

func Test(t *testing.T) {
	tests := []struct {
		name string
		file string
		want string
	}{
		{
			name: "EmptyPath",
			file: "",
			want: ".",
		},
		{
			name: "CurrentDirectory",
			file: "foo",
			want: ".",
		},
		{
			name: "RelativePath",
			file: "foo/bar",
			want: "foo",
		},
		{
			name: "NestedAbsolutePath",
			file: "/foo/bar/baz",
			want: "/foo/bar",
		},
	}

	t.Run("Open", func(t *testing.T) {
		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				t.Run("Success", func(t *testing.T) {
					var countMkdirAll int
					var countOpenFile int

					wantFile, _ := afero.TempFile(afero.NewMemMapFs(), "", "")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return nil
						},
						openFile: func(file string, flag int, perm os.FileMode) (afero.File, error) {
							countOpenFile++

							if have, want := file, test.file; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := flag, int(os.O_APPEND|os.O_CREATE|os.O_WRONLY); have != want {
								t.Errorf("have %#b, want %#b", have, want)
							}

							if have, want := perm, os.FileMode(0644); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return wantFile, nil
						},
					})(fs)

					if file, err := Open(test.file); err != nil {
						t.Errorf("have %q, want nil", err)
					} else if file != wantFile {
						t.Errorf("have %#v, want %#v", file, wantFile)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}

					if countOpenFile != 1 {
						t.Errorf("fs.OpenFile() called %d times, want exactly once", countOpenFile)
					}
				})

				t.Run("AlreadyExists", func(t *testing.T) {
					var countMkdirAll int
					var countOpenFile int

					wantFile, _ := afero.TempFile(afero.NewMemMapFs(), "", "")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return os.ErrExist
						},
						openFile: func(file string, flag int, perm os.FileMode) (afero.File, error) {
							countOpenFile++

							if have, want := file, test.file; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := flag, int(os.O_APPEND|os.O_CREATE|os.O_WRONLY); have != want {
								t.Errorf("have %#b, want %#b", have, want)
							}

							if have, want := perm, os.FileMode(0644); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return wantFile, nil
						},
					})(fs)

					if file, err := Open(test.file); err != nil {
						t.Errorf("have %q, want nil", err)
					} else if file != wantFile {
						t.Errorf("have %#v, want %#v", file, wantFile)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}

					if countOpenFile != 1 {
						t.Errorf("fs.OpenFile() called %d times, want exactly once", countOpenFile)
					}
				})

				t.Run("MkdirFailure", func(t *testing.T) {
					var countMkdirAll int

					wantError := errors.New("expected error")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return wantError
						},
					})(fs)

					if file, err := Open(test.file); err != wantError {
						t.Errorf("have %q, want %q", err, wantError)
					} else if file != nil {
						t.Errorf("have %#v, want nil", file)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}
				})
			})
		}
	})

	t.Run("Create", func(t *testing.T) {
		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				t.Run("Success", func(t *testing.T) {
					var countMkdirAll int
					var countOpenFile int

					wantFile, _ := afero.TempFile(afero.NewMemMapFs(), "", "")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return nil
						},
						create: func(file string) (afero.File, error) {
							countOpenFile++

							if have, want := file, test.file; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							return wantFile, nil
						},
					})(fs)

					if file, err := Create(test.file); err != nil {
						t.Errorf("have %q, want nil", err)
					} else if file != wantFile {
						t.Errorf("have %#v, want %#v", file, wantFile)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}

					if countOpenFile != 1 {
						t.Errorf("fs.OpenFile() called %d times, want exactly once", countOpenFile)
					}
				})

				t.Run("AlreadyExists", func(t *testing.T) {
					var countMkdirAll int
					var countOpenFile int

					wantFile, _ := afero.TempFile(afero.NewMemMapFs(), "", "")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return os.ErrExist
						},
						create: func(file string) (afero.File, error) {
							countOpenFile++

							if have, want := file, test.file; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							return wantFile, nil
						},
					})(fs)

					if file, err := Create(test.file); err != nil {
						t.Errorf("have %q, want nil", err)
					} else if file != wantFile {
						t.Errorf("have %#v, want %#v", file, wantFile)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}

					if countOpenFile != 1 {
						t.Errorf("fs.OpenFile() called %d times, want exactly once", countOpenFile)
					}
				})

				t.Run("MkdirFailure", func(t *testing.T) {
					var countMkdirAll int

					wantError := errors.New("expected error")

					defer mockAfero(mockFs{
						t: t,
						mkdirAll: func(name string, perm os.FileMode) error {
							countMkdirAll++

							if have, want := name, test.want; have != want {
								t.Errorf("have %q, want %q", have, want)
							}

							if have, want := perm, os.FileMode(0755); have != want {
								t.Errorf("have %#o, want %#o", have, want)
							}

							return wantError
						},
					})(fs)

					if file, err := Create(test.file); err != wantError {
						t.Errorf("have %q, want %q", err, wantError)
					} else if file != nil {
						t.Errorf("have %#v, want nil", file)
					}

					if countMkdirAll != 1 {
						t.Errorf("fs.MkdirAll() called %d times, want exactly once", countMkdirAll)
					}
				})
			})
		}
	})
}

func TestExists(t *testing.T) {
	t.Skip("TODO: write tests")

	tests := []struct {
		name string
		from string
		want bool
	}{
		{
			name: "",
			from: "",
			want: false,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			fs.Lock()
			defer fs.Unlock()
		})
	}
}

func TestEdit(t *testing.T) {
	t.Skip("TODO: mock exec.Command")
}
