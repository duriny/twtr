// Package paths is an internal package of twtr/cmd.
//
// It provides helpers to manipulate paths and directories.
package paths

import (
	"errors"
	"os"
	"path/filepath"
	"runtime"
	"strings"
)

// tilde leader for ExpandTilde() and ContractTilde()
const tilde = "~/"

// ExpandTilde converts ~/path/to/file to /home/user/path/to/file.
func ExpandTilde(dir string) string {
	// account for using tilde as a flat alias for $HOME
	if dir == "~" {
		dir += "/"
	}

	if home, err := os.UserHomeDir(); err == nil && strings.HasPrefix(dir, tilde) {
		dir = filepath.Join(home, dir[len(tilde):])
	}

	return dir
}

// ContractTilde converts /home/user/path/to/file to ~/path/to/file.
func ContractTilde(dir string) string {
	// get the user home dir and expand the tilde
	if home, err := os.UserHomeDir(); err == nil && strings.HasPrefix(dir, home) {
		dir = filepath.Join(tilde, dir[len(home):])
	}

	// account for using tilde as a flat alias for $HOME
	if dir == "~" {
		dir += "/"
	}

	return dir
}

// DataDir gets the XDG_DATA_DIR from the environment, with reasonable defaults
// for the current GOOS.
func DataDir() (dir string, err error) {
	// get any XDG override value
	if dir = os.Getenv("XDG_DATA_HOME"); dir != "" {
		return
	}

	// get the home dir for later
	if dir, err = os.UserHomeDir(); err != nil {
		return
	}

	// determine the rest by GOOS
	switch runtime.GOOS {
	case "windows":
		if dir = os.Getenv("LocalAppData"); dir == "" {
			err = errors.New("%LOCALAPPDATA% is not defined")
		}

	case "darwin", "ios":
		dir += "/Library/Application Support"

	case "plan9":
		dir += "/lib"

	default: // to UNIX
		dir += "/.local/share"
	}

	return
}
