package paths

import (
	"os"
	"testing"
)

func TestExpandTilde(t *testing.T) {
	home, err := os.UserHomeDir()
	if err != nil {
		t.Error(err)
	}

	tests := []struct {
		name string
		from string
		want string
	}{
		{
			name: "EmptyDir",
			from: "",
			want: "",
		},
		{
			name: "Tilde",
			from: "~",
			want: home,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if have, want := ExpandTilde(test.from), test.want; have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}

func TestContractTilde(t *testing.T) {
	home, err := os.UserHomeDir()
	if err != nil {
		t.Error(err)
	}

	tests := []struct {
		name string
		from string
		want string
	}{
		{
			name: "EmptyDir",
			from: "",
			want: "",
		},
		{
			name: "Tilde",
			from: home,
			want: "~/",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if have, want := ContractTilde(test.from), test.want; have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}

func TestDataDir(t *testing.T) {
	t.Skip("TODO: mock runtime.GOOS")
}
