package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"os/user"
	"strings"

	"duriny.envs.sh/twtr/internal/files"
	"duriny.envs.sh/twtr/internal/paths"
	"duriny.envs.sh/twtr/internal/ui"
	twtr "duriny.envs.sh/twtr/pkg"
	"github.com/spf13/cobra"
)

// File to write tweets to and read metadata from.
//
// If unset, defaults to $XDG_DATA_HOME/twtr/twtxt.txt, on UNIX systems this is
// usually under ~/.local/share, except on macOS, where it is under
// ~/Library/Application Support. On windows, the file will be created in
// %LOCALAPPDATA%.
var File string

// PreTweetHook is the default location of the hook to run before tweeting.
//
// If unset, defaults to $XDG_CONFIG_HOME/twtr/hooks/pre-tweet, see the
// os.UserConfigDir() for more information on how this is determined.
var PreTweetHook string

// PostTweetHook is the default location of the hook to run after tweeting.
//
// If unset, defaults to $XDG_CONFIG_HOME/twtr/hooks/pre-tweet, see the
// os.UserConfigDir() for more information on how this is determined.
var PostTweetHook string

// rootCmd is the root command, all other commands are assigned using the
// AddCommand() function.
var rootCmd = &cobra.Command{
	// Show basic info
	Use:   "twtr",
	Short: "A twtxt client for all your twting needs",
	Long:  `A decentralised microblogging client for hackers.`,

	// Hide the shell completion command and flags
	CompletionOptions: cobra.CompletionOptions{
		DisableNoDescFlag: true,
		HiddenDefaultCmd:  true,
	},

	// Hide the "[flags]" from the usage line, write it manually
	DisableFlagsInUseLine: true,

	// Determine the location of all the config and hook files
	PersistentPreRunE: func(cmd *cobra.Command, args []string) error {
		// if the local file is unset, get the XDG_DATA_DIR location
		if File == "" {
			dir, err := paths.DataDir()
			if err != nil {
				return fmt.Errorf("cannot get XDG_DATA_DIR: %w", err)
			}

			File = dir + "/twtr/twtxt.txt"
		}

		// if either hook file is unset, get the XDG_CONFIG_DIR location
		if PreTweetHook == "" || PostTweetHook == "" {
			dir, err := os.UserConfigDir()
			if err != nil {
				return fmt.Errorf("cannot get XDG_CONFIG_HOME: %w", err)
			}

			if PreTweetHook == "" {
				PreTweetHook = dir + "/twtr/hooks/pre-tweet"
			}

			if PostTweetHook == "" {
				PostTweetHook = dir + "/twtr/hooks/post-tweet"
			}
		}

		return nil
	},
}

// versionCmd is the custom version command, it just prints the current version
// and exists.
var versionCmd = &cobra.Command{
	// command info
	Use:   "version",
	Short: "Show the twtr version",
	Long:  "Uses semantic versioning, see https://semver.org/",

	// command main
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Println(cmd.Root().Name(), "version", twtr.Version)
	},

	// make the command super minimal
	DisableFlagParsing:    true,
	DisableFlagsInUseLine: true,
	CompletionOptions: cobra.CompletionOptions{
		DisableDefaultCmd:   true,
		DisableNoDescFlag:   true,
		DisableDescriptions: true,
		HiddenDefaultCmd:    true,
	},
}

// initCmd is a initialisation command, it allows for user defined metadata to
// be generated to the feed file.
var initCmd = &cobra.Command{
	// Show basic info
	Use:   "init [-h|--help] [--file FILE]",
	Short: "Initialise twtr locally",
	Long: `Initialise twtr locally.

The init command initialises a new twtxt.txt file for the user, the twtxt file
location defaults to $XDG_DATA_HOME/twtr/twtxt.txt, but this may be overridden
with the --file flag.`,

	// Hide the shell completion command and flags
	CompletionOptions: cobra.CompletionOptions{
		DisableNoDescFlag: true,
		HiddenDefaultCmd:  true,
	},

	// Hide the "[flags]" from the usage line, write it manually
	DisableFlagsInUseLine: true,

	// command main
	RunE: func(_ *cobra.Command, args []string) (err error) {
		// check if a previous local file exists
		if exists, err := files.Exists(File); err != nil {
			return err
		} else if exists {
			if overwrite, err := ui.GetInputBool("Are you sure you want to overwrite your existing feed?", false); err != nil {
				return err
			} else if !overwrite {
				return nil
			}
		}

		// create a default feed
		feed := twtr.Feed{
			Source: twtr.Source{
				Nick: "", // tries to get os username
				URI:  "https://example.com/~username/twtxt.txt",
			},
			Description: "I am a twtr user!",
			Follows: map[string]string{
				"twtr_news": "https://duriny.envs.sh/twtr/news.txt",
			},
		}

		// get the current user's username if it exists, good default nick
		if user, err := user.Current(); err == nil {
			feed.Nick = user.Username
		}

		// ask the user for their name
		if feed.Nick, err = ui.GetInput("What is your name?", feed.Nick); err != nil {
			return
		}

		// ask the user what they want to do as a fun first description
		if feed.Description, err = ui.GetInput("Introduce yourself", feed.Description); err != nil {
			return
		}

		// ask the user where they host their feed
		if feed.URI, err = ui.GetInput("Where will you host you're twtxt feed?", feed.URI); err != nil {
			return
		}

		var file io.WriteCloser
		if file, err = files.Create(File); err != nil {
			return
		}

		// close the local file when we're done here
		defer file.Close()

		// write the metadata and a new "Hello World!" tweet to the local file
		_, err = feed.WriteTo(file)

		return
	},
}

var postCmd = &cobra.Command{
	// Show basic info
	Use:   "post [-h|--help] [--file FILE] TWEET ...",
	Short: "Post a new tweet",
	Long: `Post a new tweet

The post command writes a new post to the specified twtxt.txt file and runs the
pre/post tweet hooks.`,

	// Allow old twtr syntax, twtr tweet "Hello World"
	Aliases: []string{"tweet"},

	// Hide the shell completion command and flags
	CompletionOptions: cobra.CompletionOptions{
		DisableNoDescFlag: true,
		HiddenDefaultCmd:  true,
	},

	// Hide the "[flags]" from the usage line, write it manually
	DisableFlagsInUseLine: true,

	// Execute the user defined pre run hook
	PreRunE: func(*cobra.Command, []string) error {
		if info, err := os.Stat(PreTweetHook); err == nil && info.Mode()&0111 == 0111 {
			cmd := exec.Command(PreTweetHook)
			cmd.Stdin = os.Stdin
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			return cmd.Run()
		}

		return nil
	},

	// Execute the user defined post run hook
	PostRunE: func(*cobra.Command, []string) error {
		if info, err := os.Stat(PostTweetHook); err == nil && info.Mode()&0111 == 0111 {
			cmd := exec.Command(PostTweetHook)
			cmd.Stdin = os.Stdin
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr

			return cmd.Run()
		}

		return nil
	},

	// Execute command main
	RunE: func(_ *cobra.Command, args []string) error {
		// open the file with writing permissions
		file, err := files.Open(File)
		if err != nil {
			return err
		}

		defer file.Close()

		// construct a twtxt client to preform the post
		client := &twtr.Client{
			// use the file we just opened as the tweet feed
			Feed: file,
		}

		var tweet string
		if len(args) > 0 {
			// join trailing command line args to form tweet
			tweet = strings.Join(args, " ")
		} else {
			// open editor and get tweet from tmp file
			tmp, err := os.CreateTemp("", "twtxt_*.txt")
			if err != nil {
				return err
			}

			// defer clean up
			defer os.Remove(tmp.Name())
			defer tmp.Close()

			// edit the tmp file
			if err := files.Edit(tmp.Name()); err != nil {
				return err
			}

			// read the tmp file as a string to tweet
			if data, err := os.ReadFile(tmp.Name()); err != nil {
				return err
			} else {
				tweet = string(data)
			}
		}

		// treat all args as words of the tweet to post
		return client.Post(tweet)
	},
}

// init registers flags and subcommands to the root command.
func init() {
	rootCmd.PersistentFlags().StringVar(&File, "file", File, "local twtxt file")
	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(initCmd)
	rootCmd.AddCommand(postCmd)
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}
