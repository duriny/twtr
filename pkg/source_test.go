package twtr_test

import (
	"testing"

	. "duriny.envs.sh/twtr/pkg"
)

func TestSourceString(t *testing.T) {
	tests := []struct {
		name string
		want string
		from Source
	}{
		{
			name: "EmptySource",
			want: "",
			from: Source{
				Nick: "",
				URI:  "",
			},
		},
		{
			name: "NickOnly",
			want: "",
			from: Source{
				Nick: "~duriny",
				URI:  "",
			},
		},
		{
			name: "URIOnly",
			want: "@<https://envs.net/~duriny/twtxt.txt>",
			from: Source{
				Nick: "",
				URI:  "https://envs.net/~duriny/twtxt.txt",
			},
		},
		{
			name: "FullSource",
			want: "@<~duriny https://envs.net/~duriny/twtxt.txt>",
			from: Source{
				Nick: "~duriny",
				URI:  "https://envs.net/~duriny/twtxt.txt",
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if have, want := test.from.String(), test.want; have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}
