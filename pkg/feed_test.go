package twtr_test

import (
	"bytes"
	"strings"
	"testing"
	"time"

	. "duriny.envs.sh/twtr/pkg"
	"github.com/google/go-cmp/cmp"
)

func TestFeed(t *testing.T) {
	tests := []struct {
		name string
		want string
		from Feed
	}{
		{
			name: "NoBio",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# Following:",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Follows: map[string]string{
					"twtr_news": "https://duriny.envs.sh/twtr/news.txt",
				},
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
		{
			name: "NoNick",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# url         = https://envs.net/~duriny/twtxt.txt",
				"# description = Author of twtr",
				"#",
				"# Following:",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Source: Source{
					URI: "https://envs.net/~duriny/twtxt.txt",
				},
				Description: "Author of twtr",
				Follows: map[string]string{
					"twtr_news": "https://duriny.envs.sh/twtr/news.txt",
				},
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
		{
			name: "NoURI",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# nick        = ~duriny",
				"# description = Author of twtr",
				"#",
				"# Following:",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Source: Source{
					Nick: "~duriny",
				},
				Description: "Author of twtr",
				Follows: map[string]string{
					"twtr_news": "https://duriny.envs.sh/twtr/news.txt",
				},
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
		{
			name: "NoDescription",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# nick = ~duriny",
				"# url  = https://envs.net/~duriny/twtxt.txt",
				"#",
				"# Following:",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Source: Source{
					Nick: "~duriny",
					URI:  "https://envs.net/~duriny/twtxt.txt",
				},
				Follows: map[string]string{
					"twtr_news": "https://duriny.envs.sh/twtr/news.txt",
				},
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
		{
			name: "NoFollowing",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# nick        = ~duriny",
				"# url         = https://envs.net/~duriny/twtxt.txt",
				"# description = Author of twtr",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Source: Source{
					Nick: "~duriny",
					URI:  "https://envs.net/~duriny/twtxt.txt",
				},
				Description: "Author of twtr",
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
		{
			name: "FullFeed",
			want: strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# nick        = ~duriny",
				"# url         = https://envs.net/~duriny/twtxt.txt",
				"# description = Author of twtr",
				"#",
				"# Following:",
				"# follow = computerphile https://feeds.twtxt.net/computerphile/twtxt.txt",
				"# follow = nzherald-co-nz-world https://feeds.twtxt.net/nzherald-co-nz-world/twtxt.txt",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"# follow = twtxt_news https://buckket.org/twtxt_news.txt",
				"# follow = xkcd https://feeds.twtxt.net/xkcd/twtxt.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
			}, "\n"),
			from: Feed{
				Source: Source{
					Nick: "~duriny",
					URI:  "https://envs.net/~duriny/twtxt.txt",
				},
				Description: "Author of twtr",
				Follows: map[string]string{
					"computerphile":        "https://feeds.twtxt.net/computerphile/twtxt.txt",
					"nzherald-co-nz-world": "https://feeds.twtxt.net/nzherald-co-nz-world/twtxt.txt",
					"twtr_news":            "https://duriny.envs.sh/twtr/news.txt",
					"twtxt_news":           "https://buckket.org/twtxt_news.txt",
					"xkcd":                 "https://feeds.twtxt.net/xkcd/twtxt.txt",
				},
				Tweets: Tweets{
					Tweet{
						Time: time.Date(2022, 03, 29, 10, 42, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
						Text: "Hello World!",
					},
				},
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Run("String()", func(t *testing.T) {
				if diff := cmp.Diff(test.want, test.from.String()); diff != "" {
					t.Error(diff)
				}
			})

			t.Run("WriteTo()", func(t *testing.T) {
				var buf bytes.Buffer

				if n, err := test.from.WriteTo(&buf); n != int64(len(test.want)+1) || err != nil {
					t.Errorf("have (n: %d, err: %q), want (n: %d, nil)", n, err, len(test.want)+1)
				}

				if diff := cmp.Diff(test.want+"\n", buf.String()); diff != "" {
					t.Error(diff)
				}
			})
		})
	}
}
