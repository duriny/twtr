//go:build test_fetch
// +build test_fetch

package twtr

import (
	"context"
	"errors"
	"io"
	"testing"
	"time"
)

func TestClientFetch(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping long test")
	}

	t.Parallel()

	tests := []struct {
		name    string
		uri     string
		timeout time.Duration
	}{
		{
			name:    "HttpNoTimeOut",
			uri:     "http://envs.net",
			timeout: 0,
		},
		{
			name:    "HttpsNoTimeOut",
			uri:     "https://envs.net",
			timeout: 0,
		},
		{
			name:    "GopherNoTimeOut",
			uri:     "gopher://envs.net/0/~aesophod/welcome.txt", // first user w/ gopher file
			timeout: 0,
		},
		{
			name:    "GeminiNoTimeOut",
			uri:     "gemini://envs.net",
			timeout: 0,
		},
		{
			name:    "HttpWithTimeOut",
			uri:     "http://envs.net",
			timeout: time.Hour,
		},
		{
			name:    "HttpsWithTimeOut",
			uri:     "https://envs.net",
			timeout: time.Hour,
		},
		{
			name:    "GopherWithTimeOut",
			uri:     "gopher://envs.net/0/~aesophod/welcome.txt", // first user w/ gopher file
			timeout: time.Hour,
		},
		{
			name:    "GeminiWithTimeOut",
			uri:     "gemini://envs.net",
			timeout: time.Hour,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			client := &Client{Timeout: test.timeout}

			data, err := client.fetch(test.uri)
			if err != nil {
				t.Error(err)
			}

			if data != nil {
				if data, err := io.ReadAll(data); err != nil {
					t.Error(err)
				} else {
					_ = data

					// for debugging
					// os.Stdout.Write(data)
				}

				if err := data.Close(); err != nil {
					t.Error(err)
				}
			}
		})
	}

	t.Run("EmptyURI", func(t *testing.T) {
		t.Parallel()

		client := &Client{}

		data, err := client.fetch("")

		if data != nil {
			t.Errorf("have %#v, want nil", data)
		}

		if want := "Get \"\": unsupported protocol scheme \"\""; err == nil {
			t.Errorf("have nil, want %q", want)
		} else if have := err.Error(); have != want {
			t.Errorf("have %q, want %q", have, want)
		}
	})
}

func TestClientFetchHTTP(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping long test")
	}

	t.Parallel()

	tests := []struct {
		name string
		uri  string
		ctx  context.Context
		want error
	}{
		{
			name: "HttpNoTimeOut",
			uri:  "http://envs.net",
			ctx:  context.Background(),
			want: nil,
		},
		{
			name: "HttpsNoTimeOut",
			uri:  "https://envs.net",
			ctx:  context.Background(),
			want: nil,
		},
		{
			name: "HttpWithTimeOut",
			uri:  "http://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Hour)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: nil,
		},
		{
			name: "HttpsWithTimeOut",
			uri:  "https://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Hour)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: nil,
		},
		{
			name: "HttpNotOK",
			uri:  "http://envs.net/path/to/file/that/does/not/exist",
			ctx:  context.Background(),
			want: errors.New("http://envs.net/path/to/file/that/does/not/exist not OK: \"404 Not Found\""),
		},
		{
			name: "NilContext",
			uri:  "https://envs.net",
			ctx:  nil,
			want: errors.New("net/http: nil Context"),
		},
		{
			name: "ContextTimesOut",
			uri:  "https://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: errors.New("Get \"https://envs.net\": context deadline exceeded"),
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			client := &Client{}

			data, err := client.fetchHTTP(test.ctx, test.uri)

			if test.want == nil {
				if err != nil {
					t.Error(err)
				}

				if data != nil {
					if data, err := io.ReadAll(data); err != nil {
						t.Error(err)
					} else {
						_ = data

						// for debugging
						// os.Stdout.Write(data)
					}

					if err := data.Close(); err != nil {
						t.Error(err)
					}
				}
			} else if want := test.want.Error(); err == nil {
				t.Errorf("have nil, want %q", want)
			} else if have := err.Error(); have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}

func TestClientFetchGopher(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping long test")
	}

	t.Parallel()

	tests := []struct {
		name string
		uri  string
		ctx  context.Context
		want error
	}{
		{
			name: "GopherNoTimeOut",
			uri:  "gopher://envs.net/0/~aesophod/welcome.txt", // first user w/ gopher file
			ctx:  context.Background(),
			want: nil,
		},
		{
			name: "GopherWithTimeOut",
			uri:  "gopher://envs.net/0/~aesophod/welcome.txt", // first user w/ gopher file
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Hour)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: nil,
		},
		{
			name: "ResponseHasNoBody",
			uri:  "gopher://envs.net", // root has no file, only dir
			ctx:  context.Background(),
			want: errors.New("gopher://envs.net has no body: 49 (DIR)"),
		},
		{
			name: "NilContext",
			uri:  "gopher://envs.net",
			ctx:  nil,
			want: errors.New("nil context"),
		},
		{
			name: "ContextTimesOut",
			uri:  "gopher://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: errors.New("context deadline exceeded"),
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			client := &Client{}

			data, err := client.fetchGopher(test.ctx, test.uri)

			if test.want == nil {
				if err != nil {
					t.Error(err)
				}

				if data != nil {
					if data, err := io.ReadAll(data); err != nil {
						t.Error(err)
					} else {
						_ = data

						// for debugging
						// os.Stdout.Write(data)
					}

					if err := data.Close(); err != nil {
						t.Error(err)
					}
				}
			} else if want := test.want.Error(); err == nil {
				t.Errorf("have nil, want %q", want)
			} else if have := err.Error(); have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}

func TestClientFetchGemini(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping long test")
	}

	t.Parallel()

	tests := []struct {
		name string
		uri  string
		ctx  context.Context
		want error
	}{
		{
			name: "GeminiNoTimeOut",
			uri:  "gemini://envs.net",
			ctx:  context.Background(),
		},
		{
			name: "GeminiWithTimeOut",
			uri:  "gemini://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Hour)

				t.Cleanup(cancel)

				return ctx
			}(),
		},
		{
			name: "NilContext",
			uri:  "gemini://envs.net",
			ctx:  nil,
			want: errors.New("nil context"),
		},
		{
			name: "ContextTimesOut",
			uri:  "gemini://envs.net",
			ctx: func() context.Context {
				ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond)

				t.Cleanup(cancel)

				return ctx
			}(),
			want: errors.New("dial tcp: lookup envs.net: i/o timeout"),
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			client := &Client{}

			data, err := client.fetchGemini(test.ctx, test.uri)

			if test.want == nil {
				if err != nil {
					t.Error(err)
				}

				if data != nil {
					if data, err := io.ReadAll(data); err != nil {
						t.Error(err)
					} else {
						_ = data

						// for debugging
						// os.Stdout.Write(data)
					}

					if err := data.Close(); err != nil {
						t.Error(err)
					}
				}
			} else if want := test.want.Error(); err == nil {
				t.Errorf("have nil, want %q", want)
			} else if have := err.Error(); have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	}
}
