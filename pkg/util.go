package twtr

import (
	"context"
	"fmt"
	"io"
	"strings"

	"go.uber.org/multierr"
)

// closerFunc is an io.Closer that wraps another io.Closer
type closerFunc func() error

// Close implements io.Closer
func (fn closerFunc) Close() error {
	return fn()
}

// wrappedReadCloser is an io.ReadCloser that wraps another io.Closer.
type wrappedReadCloser struct {
	io.ReadCloser
	io.Closer
}

// Read implements io.Reader
func (wrapped wrappedReadCloser) Read(p []byte) (int, error) {
	return wrapped.ReadCloser.Read(p)
}

// Close implements io.Closer
func (wrapped wrappedReadCloser) Close() error {
	return multierr.Combine(
		wrapped.ReadCloser.Close(),
		wrapped.Closer.Close(),
	)
}

// appendCloser appends another io.Closer to a given io.ReadCloser, it allows
// multiple closers to be closed with a single close call.
func appendCloser(readCloser io.ReadCloser, closer io.Closer) io.ReadCloser {
	return wrappedReadCloser{readCloser, closer}
}

// appendCloserFunc appends another io.Closer to a given io.ReadCloser, it
// allows multiple closers to be closed with a single close call.
func appendCloserFunc(readCloser io.ReadCloser, closer closerFunc) io.ReadCloser {
	return appendCloser(readCloser, closer)
}

// appendCloserCancelFunc appends another io.Closer to a given io.ReadCloser, it
// allows multiple closers to be closed with a single close call.
func appendCloserCancelFunc(readCloser io.ReadCloser, cancel context.CancelFunc) io.ReadCloser {
	return appendCloserFunc(readCloser, func() error { cancel(); return nil })
}

// buildMetadataBlock with the given title.
//
// Attempts to retrieve values by the given fields, in the order given,
// constructs a comment block for that metadata.
//
//     # <TITLE>:
//     # field1 = value1
//     # field2 = value2
//     # field3 = value3
//     #
//
// Note that a single empty comment line is trailing, if no values could be
// extracted, then an empty string is returned, even if the title is given
func buildMetadataBlock(title string, fields []string, values map[string][]string) string {
	// found fields will be added to the block
	found := []string{}

	// usable values that were found AND have at least one value attached
	usable := map[string][]string{}

	// pad the found values evenly
	padding := 0

	// search the fields for valid values and calculate padding
	for _, field := range fields {
		// skip any values that don't exist, or are empty
		if values, ok := values[field]; !ok {
			continue
		} else {
			for _, value := range values {
				value := strings.TrimSpace(value)

				if value != "" {
					usable[field] = append(usable[field], value)
				}
			}

			// skip this field if there aren't actually any usable values
			if len(usable[field]) <= 0 {
				continue
			}
		}

		// expand the padding to fit the longest field
		if l := len(field); l > padding {
			padding = l
		}

		// mark the fields as found
		found = append(found, field)
	}

	// if no values were found, the block is empty
	if len(found) <= 0 {
		return ""
	}

	// there will be at least as many lines as found fields
	lines := []string{}

	// add the title, if given
	if title != "" {
		lines = append(lines, "# "+strings.TrimSpace(title)+":")
	}

	// add each found field to the block
	for _, field := range found {
		for _, value := range usable[field] {
			lines = append(lines, fmt.Sprintf("# %-*s = %s", padding, field, value))
		}
	}

	// add a spacer line after the fields
	lines = append(lines, "#")

	// join the lines into a block
	return strings.Join(lines, "\n")
}
