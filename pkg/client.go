package twtr

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"duriny.envs.sh/twtr/pkg/parse"
	"git.mills.io/prologic/go-gopher"
	"git.sr.ht/~adnano/go-gemini"
)

// Version of the twtr client.
const Version = "v0.2.5"

// Client is an instance of the twtr client.
//
// Configure the client as desired.
type Client struct {
	// Timeout for all network activity, defaults to no timeout.
	Timeout time.Duration

	// Feed to write tweets to, if unset, tweets will be discarded.
	Feed io.Writer
}

// Post a new tweet on this client, running the pre/post tweet hooks along the
// way, return any error that occurs.
func (client *Client) Post(tweet string) (err error) {
	// trim dangling new lines and other whitespace
	tweet = strings.TrimSpace(tweet)

	// if the tweet is empty or just whitespace, then don't post anything
	if tweet == "" {
		return
	}

	// if the feed is empty, then don't post anything
	if client.Feed == nil {
		return
	}

	// write the tweet to the feed
	_, err = NewTweet(tweet).WriteTo(client.Feed)

	return
}

// Fetch a twtxt feed from the given source.
//
// Returns any error that occurs while fetching or parsing the feed.
func (client *Client) Fetch(source Source) (feed Feed, err error) {
	// the feed's source is known
	feed.Source = source

	// fetch the raw data
	var data io.ReadCloser
	if data, err = client.fetch(source.URI); err != nil {
		return
	}

	// close the data when we're done
	defer data.Close()

	// parse the lines in the feed
	for line := range parse.Feed(data) {
		if comment, ok := line.AsCommentLine(); ok {
			switch comment.FIELD {
			// accumulate description
			case "description":
				if feed.Description == "" {
					feed.Description = comment.VALUE
				}

			// accumulate other feeds to follow
			case "follow":
				if fields := strings.Fields(comment.VALUE); len(fields) > 1 {
					feed.Follows[fields[0]] = strings.Join(fields[1:], " ")
				}
			}
		} else if tweet, ok := line.AsTweetLine(); ok {
			if err = tweet.Err(); err != nil {
				return
			}

			feed.Tweets = append(feed.Tweets, Tweet{
				Source: source,
				Time:   tweet.TIMESTAMP,
				Text:   tweet.MESSAGE,
			})
		}
	}

	return
}

// fetch remote data over any supported protocol, returns any error that occurs
// while fetching the data.
func (client *Client) fetch(uri string) (data io.ReadCloser, err error) {
	// construct context, determine timeout
	ctx, cancel := context.Background(), context.CancelFunc(func() {})
	if client.Timeout > 0 {
		ctx, cancel = context.WithTimeout(ctx, client.Timeout)
	}

	// determine which protocol to attempt to use
	switch {
	case strings.HasPrefix(uri, "gopher://"):
		data, err = client.fetchGopher(ctx, uri)

	case strings.HasPrefix(uri, "gemini://"):
		data, err = client.fetchGemini(ctx, uri)

	default:
		data, err = client.fetchHTTP(ctx, uri)
	}

	// include cancel func in data closer, or cancel now if an error occurred
	if err != nil {
		cancel()
	} else {
		data = appendCloserCancelFunc(data, cancel)
	}

	return
}

// fetchHTTP is a helper of fetch().
//
// Standard HTTP GET request, sets UserAgent to send client version, for privacy
// reasons, twtr will not send the feed url as part of the UserAgent.
func (client *Client) fetchHTTP(ctx context.Context, uri string) (data io.ReadCloser, err error) {
	// construct an HTTP GET request
	req, err := http.NewRequestWithContext(ctx, "GET", uri, nil)
	if err != nil {
		return nil, err
	}

	// set UserAgent header
	req.Header.Set("User-Agent", "twtr/"+Version)

	// use an HTTP client with the given timeout to make the request
	resp, err := (&http.Client{}).Do(req)
	if err != nil {
		return nil, err
	}

	// check that the request succeeded
	if resp.StatusCode != http.StatusOK {
		// response body must be read till EOF and closed
		io.ReadAll(resp.Body)
		resp.Body.Close()

		return nil, fmt.Errorf("%s not OK: %q", uri, resp.Status)
	}

	// return the response body
	return resp.Body, nil
}

// fetchGopher is a helper of fetch().
//
// For twtxt.txt feeds served over the gopher protocol.
func (client *Client) fetchGopher(ctx context.Context, uri string) (data io.ReadCloser, err error) {
	if ctx == nil {
		return nil, errors.New("nil context")
	}

	done := make(chan struct{}, 1)

	// get the request in a go routine
	go func() {
		var resp *gopher.Response

		// get the response
		if resp, err = gopher.Get(uri); err == nil {
			if data = resp.Body; data == nil {
				err = fmt.Errorf("%s has no body: %d (%s)", uri, resp.Type, resp.Type)
			}
		}

		// signal that the response was retrieved correctly
		done <- struct{}{}
	}()

	// listen to the completion or timeout of the context
	select {
	case <-ctx.Done():
		return nil, ctx.Err()

	case <-done:
		return
	}
}

// fetchGemini is a helper of fetch().
//
// For twtxt.txt feeds served over the gemini protocol
func (client *Client) fetchGemini(ctx context.Context, uri string) (data io.ReadCloser, err error) {
	if ctx == nil {
		return nil, errors.New("nil context")
	}

	// construct and execute gemini request
	resp, err := (&gemini.Client{}).Get(ctx, uri)
	if err != nil {
		return nil, err
	}

	// return the response body
	return resp.Body, nil
}
