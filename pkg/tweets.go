package twtr

import (
	"strings"
)

// Tweets are a collection of tweets.
type Tweets []Tweet

// Len reports the number of tweets.
func (tweets Tweets) Len() int {
	return len(tweets)
}

// Less reports whether the tweet at index i was posted before the tweet at j.
func (tweets Tweets) Less(i, j int) bool {
	return tweets[i].Time.Before(tweets[j].Time)
}

// Swap the tweet with index i for the tweet with index j.
func (tweets Tweets) Swap(i, j int) {
	tweets[i], tweets[j] = tweets[j], tweets[i]
}

// String formats the tweets as a string representing all the tweets according
// to the twtxt.txt file specification.
func (tweets Tweets) String() string {
	lines := make([]string, len(tweets))

	for i, tweet := range tweets {
		lines[i] = tweet.String()
	}

	return strings.Join(lines, "\n")
}
