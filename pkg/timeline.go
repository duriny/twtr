package twtr

import "strings"

// Timeline of feeds.
type Timeline []Feed

// Tweets of the timeline's feeds.
func (timeline Timeline) Tweets() Tweets {
	tweets := make(Tweets, 0)

	for _, feed := range timeline {
		tweets = append(tweets, feed.Tweets...)
	}

	return tweets
}

// String formats the tweets of the timeline according to the twtxt registry
// format specification.
//
//     NICK\tURL\tTIMESTAMP\tMESSAGE
//
// See https://twtxt.readthedocs.io/en/latest/user/registry.html
func (timeline Timeline) String() string {
	tweets := timeline.Tweets()
	lines := make([]string, len(tweets))

	for i, tweet := range tweets {
		lines[i] = strings.Join([]string{
			tweet.Nick,
			tweet.URI,
			tweet.String(),
		}, "\t")
	}

	return strings.Join(lines, "\n")
}
