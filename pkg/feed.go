package twtr

import (
	"fmt"
	"io"
	"sort"
	"strings"
)

// Feed represents a twtxt feed file.
type Feed struct {
	// Source of the feed
	Source

	// Description of the feed.
	Description string

	// Follows these other feeds.
	Follows map[string]string

	// Tweets of the feed.
	Tweets
}

// String formats the feed as a file representing the tweets and the metadata
// specified.
func (feed Feed) String() string {
	lines := []string{
		"# twtxt is decentralised microblogging for hackers",
		"#",
		"# Learn more about twtxt at https://twtxt.readthedocs.io",
		"#",
	}

	// construct a bio block, nick, url, description etc
	if block := buildMetadataBlock("About me", []string{
		"nick",
		"url",
		"description",
	}, map[string][]string{
		"nick":        []string{feed.Nick},
		"url":         []string{feed.URI},
		"description": []string{feed.Description},
	}); block != "" {
		lines = append(lines, block)
	}

	// extract the other feeds that the feed follows
	follows := make([]string, 0, len(feed.Follows))
	for nick, uri := range feed.Follows {
		follows = append(follows, nick+" "+uri)
	}

	// sort the feeds to follow
	sort.Strings(follows)

	if block := buildMetadataBlock("Following", []string{
		"follow",
	}, map[string][]string{
		"follow": follows,
	}); block != "" {
		lines = append(lines, block)
	}

	return strings.Join(lines, "\n") + "\n" + feed.Tweets.String()
}

// WriteTo formats the feed as a file representing the tweets and the metadata
// specified.
func (feed Feed) WriteTo(w io.Writer) (n int64, err error) {
	count, err := fmt.Fprintln(w, feed)

	return int64(count), err
}
