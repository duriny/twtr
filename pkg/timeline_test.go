package twtr_test

import (
	"strings"
	"testing"
	"time"

	. "duriny.envs.sh/twtr/pkg"
	"github.com/google/go-cmp/cmp"
)

func TestTimelineTweets(t *testing.T) {
	tests := []struct {
		name string
		want Tweets
		from Timeline
	}{
		{
			name: "EmptyTimeline",
			want: Tweets{},
			from: Timeline{},
		},
		{
			name: "SingleEmptyFeed",
			want: Tweets{},
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
				},
			},
		},
		{
			name: "MultipleEmptyFeeds",
			want: Tweets{},
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_2",
						URI:  "https://envs.net/~duriny_2/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
				},
			},
		},
		{
			name: "SingleFeedWithTweets",
			want: Tweets{
				Tweet{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Hello World",
				},
				Tweet{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Welcome to twtxt",
				},
			},
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny",
								URI:  "https://envs.net/~duriny/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny",
								URI:  "https://envs.net/~duriny/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt",
						},
					},
				},
			},
		},
		{
			name: "MultipleFeedsWithTweets",
			want: Tweets{
				Tweet{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Hello World",
				},
				Tweet{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Welcome to twtxt",
				},
				Tweet{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 48, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Hello World!!!!",
				},
				Tweet{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
					Time: time.Date(2022, 03, 29, 13, 49, 18, 00, time.FixedZone("UTC+13", +13*60*60)),
					Text: "Welcome to twtxt @<~duriny_1 https://envs.net/~duriny_1/twtxt.txt>",
				},
			},
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny_1",
								URI:  "https://envs.net/~duriny_1/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny_1",
								URI:  "https://envs.net/~duriny_1/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt",
						},
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_2",
						URI:  "https://envs.net/~duriny_2/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny_3",
								URI:  "https://envs.net/~duriny_3/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 48, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World!!!!",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny_3",
								URI:  "https://envs.net/~duriny_3/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 49, 18, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt @<~duriny_1 https://envs.net/~duriny_1/twtxt.txt>",
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if diff := cmp.Diff(test.want, test.from.Tweets()); diff != "" {
				t.Errorf(diff)
			}
		})
	}
}

func TestTimelineString(t *testing.T) {
	tests := []struct {
		name string
		want string
		from Timeline
	}{
		{
			name: "EmptyTimeline",
			want: "",
			from: Timeline{},
		},
		{
			name: "SingleEmptyFeed",
			want: "",
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
				},
			},
		},
		{
			name: "MultipleEmptyFeeds",
			want: "",
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_2",
						URI:  "https://envs.net/~duriny_2/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
				},
			},
		},
		{
			name: "SingleFeedWithTweets",
			want: strings.Join([]string{
				"~duriny\thttps://envs.net/~duriny/twtxt.txt\t2022-03-29T13:44:57+13:00\tHello World",
				"~duriny\thttps://envs.net/~duriny/twtxt.txt\t2022-03-29T13:45:42+13:00\tWelcome to twtxt",
			}, "\n"),
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny",
						URI:  "https://envs.net/~duriny/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny",
								URI:  "https://envs.net/~duriny/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny",
								URI:  "https://envs.net/~duriny/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt",
						},
					},
				},
			},
		},
		{
			name: "MultipleFeedsWithTweets",
			want: strings.Join([]string{
				"~duriny_1\thttps://envs.net/~duriny_1/twtxt.txt\t2022-03-29T13:44:57+13:00\tHello World",
				"~duriny_1\thttps://envs.net/~duriny_1/twtxt.txt\t2022-03-29T13:45:42+13:00\tWelcome to twtxt",
				"~duriny_3\thttps://envs.net/~duriny_3/twtxt.txt\t2022-03-29T13:48:26+13:00\tHello World!!!!",
				"~duriny_3\thttps://envs.net/~duriny_3/twtxt.txt\t2022-03-29T13:49:18+13:00\tWelcome to twtxt @<~duriny_1 https://envs.net/~duriny_1/twtxt.txt>",
			}, "\n"),
			from: Timeline{
				Feed{
					Source: Source{
						Nick: "~duriny_1",
						URI:  "https://envs.net/~duriny_1/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny_1",
								URI:  "https://envs.net/~duriny_1/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 44, 57, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny_1",
								URI:  "https://envs.net/~duriny_1/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 45, 42, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt",
						},
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_2",
						URI:  "https://envs.net/~duriny_2/twtxt.txt",
					},
				},
				Feed{
					Source: Source{
						Nick: "~duriny_3",
						URI:  "https://envs.net/~duriny_3/twtxt.txt",
					},
					Tweets: Tweets{
						Tweet{
							Source: Source{
								Nick: "~duriny_3",
								URI:  "https://envs.net/~duriny_3/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 48, 26, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Hello World!!!!",
						},
						Tweet{
							Source: Source{
								Nick: "~duriny_3",
								URI:  "https://envs.net/~duriny_3/twtxt.txt",
							},
							Time: time.Date(2022, 03, 29, 13, 49, 18, 00, time.FixedZone("UTC+13", +13*60*60)),
							Text: "Welcome to twtxt @<~duriny_1 https://envs.net/~duriny_1/twtxt.txt>",
						},
					},
				},
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if diff := cmp.Diff(test.want, test.from.String()); diff != "" {
				t.Errorf(diff)
			}
		})
	}
}
