package twtr_test

import (
	"math/rand"
	"sort"
	"testing"
	"time"

	. "duriny.envs.sh/twtr/pkg"
)

func TestTweetsSorting(t *testing.T) {
	tweets := make(Tweets, rand.Intn(1000))

	for i := range tweets {
		tweets[i].Time = time.Date(
			rand.Intn(2022),                     // year
			time.Month(rand.Intn(12))+1,         // month
			rand.Intn(31),                       // day
			rand.Intn(24),                       // hour
			rand.Intn(60),                       // minute
			rand.Intn(60),                       // second
			rand.Intn(1_000_000_000),            // nanosecond
			time.FixedZone("UTC+13", +13*60*60), // location
		)
	}

	if sort.IsSorted(tweets) {
		t.Log(tweets)
		t.Skip("already sorted")
	}

	sort.Sort(tweets)

	if !sort.IsSorted(tweets) {
		t.Log(tweets)
		t.Error("sort failed")
	}

	sort.Sort(sort.Reverse(tweets))

	if sort.IsSorted(tweets) {
		t.Log(tweets)
		t.Error("reverse sort failed")
	}

	if !sort.IsSorted(sort.Reverse(tweets)) {
		t.Log(tweets)
		t.Error("reverse sort failed")
	}
}
