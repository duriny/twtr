package twtr_test

import (
	"bytes"
	"fmt"
	"strings"
	"testing"
	"time"

	. "duriny.envs.sh/twtr/pkg"
)

// BadWriter is an io.Writer that returns an error on every call to the write
// method.
type BadWriter struct {
	Err error
}

// Write implements the io.Writer interface and returns the error that the
// BadWriter contains.
func (w *BadWriter) Write(buf []byte) (n int, err error) {
	err = w.Err

	return
}

func TestClientPost(t *testing.T) {
	t.Parallel()

	tests := []struct {
		name string
		post string
	}{
		{
			name: "SingleLinePost",
			post: "Hello, 世界",
		},
		{
			name: "MultiLinePost",
			post: "Hello, 世界\nWelcome to twtxt!",
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			t.Run("DefaultClient", func(t *testing.T) {
				t.Parallel()

				client := &Client{}

				if err := client.Post(test.post); err != nil {
					t.Error(err)
				}
			})

			t.Run("ClientWithFeed", func(t *testing.T) {
				t.Parallel()

				var buf bytes.Buffer

				client := &Client{
					Feed: &buf,
				}

				if err := client.Post(test.post); err != nil {
					t.Error(err)
				}

				if tweet := buf.String(); tweet == "" {
					t.Error("nothing was posted to feed")
				} else if !strings.Contains(tweet, "\t") {
					t.Errorf("missing tab delimiter: %q", tweet)
				} else if parts := strings.SplitN(tweet, "\t", 2); len(parts) < 1 {
					t.Errorf("failed to split tweet: %q", tweet)
				} else if timestamp := parts[0]; timestamp == "" {
					t.Errorf("missing timestamp: %q", timestamp)
				} else if _, err := time.Parse(time.RFC3339, timestamp); err != nil {
					t.Errorf("invalid timestamp: %q", err)
				} else if have, want := parts[1], strings.NewReplacer("\n", "\u2028").Replace(test.post)+"\n"; have != want {
					t.Errorf("have %q, want %q", have, want)
				}
			})

			t.Run("WritingToFeedReturnsError", func(t *testing.T) {
				t.Parallel()

				var err error = fmt.Errorf("testing error")

				client := &Client{
					Feed: &BadWriter{Err: err},
				}

				if have, want := client.Post(test.post), err; have != want {
					t.Errorf("have %q, want %q", have, want)
				}
			})
		})
	}

	t.Run("EmptyPost", func(t *testing.T) {
		t.Run("DefaultClient", func(t *testing.T) {
			t.Parallel()

			client := &Client{}
			if err := client.Post(""); err != nil {
				t.Error(err)
			}
		})

		t.Run("ClientWithFeed", func(t *testing.T) {
			t.Parallel()

			var buf bytes.Buffer

			client := &Client{
				Feed: &buf,
			}

			if err := client.Post(""); err != nil {
				t.Error(err)
			}

			if tweet := buf.String(); tweet != "" {
				t.Error("nothing should be posted to feed")
			}
		})
	})
}

func ExampleClient_Post() {
	// construct a client
	client := &Client{
		// What happens if you just post to STDOUT? Give it a try :)
		// Feed: os.Stdout,
	}

	// post tweet to the feed
	if err := client.Post("Hello World!"); err != nil {
		fmt.Println("err:", err)
	}
	// Output:
}
