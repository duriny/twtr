package twtr

import (
	"bytes"
	"errors"
	"io"
	"testing"
	"testing/iotest"

	"github.com/google/go-cmp/cmp"
	"go.uber.org/multierr"
)

func TestCloserFunc(t *testing.T) {
	t.Run("Close()", func(t *testing.T) {
		tests := []struct {
			name string
			want error
		}{
			{
				name: "NilError",
			},
			{
				name: "NonNilError",
				want: errors.New("expected error"),
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				var times int
				var fn closerFunc = func() error {
					times++

					return test.want
				}

				if have, want := fn.Close(), test.want; have != want {
					t.Errorf("have %q, want %q", have, want)
				}

				if times != 1 {
					t.Errorf("Close() was called %d times, want exactly once", times)
				}
			})
		}
	})
}

func TestWrappedReadCloser(t *testing.T) {
	t.Run("Read()", func(t *testing.T) {
		tests := []struct {
			name string
			want []byte
		}{
			{
				name: "EmptyReader",
				want: []byte{},
			},
			{
				name: "FullReader",
				want: []byte("some data or text etc."),
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				wrapped := wrappedReadCloser{
					ReadCloser: io.NopCloser(bytes.NewReader(test.want)),
					Closer: closerFunc(func() error {
						t.Error("Close() should not have been called")

						return nil
					}),
				}

				if have, err := io.ReadAll(wrapped); err != nil {
					t.Error(err)
				} else if want := test.want; bytes.Compare(have, want) != 0 {
					t.Errorf("have %#v, want %#v", have, want)
				}
			})
		}

		t.Run("BadReader", func(t *testing.T) {
			want := errors.New("expected error")

			wrapped := wrappedReadCloser{
				ReadCloser: io.NopCloser(iotest.ErrReader(want)),
				Closer: closerFunc(func() error {
					t.Error("Close() should not have been called")

					return nil
				}),
			}

			if _, have := io.ReadAll(wrapped); have != want {
				t.Errorf("have %q, want %q", have, want)
			}
		})
	})

	t.Run("Close", func(t *testing.T) {
		tests := []struct {
			name string
			err1 error
			err2 error
		}{
			{
				name: "ReadCloserFails",
				err1: errors.New("expected read closer error"),
				err2: nil,
			},
			{
				name: "InnerCloserFails",
				err1: nil,
				err2: errors.New("expected closer error"),
			},
			{
				name: "BothReadCloserAndInnerCloserFail",
				err1: errors.New("expected read closer error"),
				err2: errors.New("expected closer error"),
			},
			{
				name: "BothReadCloserAndInnerCloserSucceed",
				err1: nil,
				err2: nil,
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				var timesReadCloserCalled, timesInnerCloserCalled int

				wrapped := wrappedReadCloser{
					ReadCloser: struct {
						io.Reader
						io.Closer
					}{
						&bytes.Buffer{},
						closerFunc(func() error {
							timesReadCloserCalled++

							return test.err1
						}),
					},
					Closer: closerFunc(func() error {
						timesInnerCloserCalled++

						return test.err2
					}),
				}

				if have, want := wrapped.Close(), multierr.Combine(
					test.err1,
					test.err2,
				); have != want && have != nil && want != nil && have.Error() != want.Error() {
					t.Errorf("have %q, want %q", have, want)
				}

				if timesReadCloserCalled != 1 {
					t.Errorf("ReadCloser.Close() was called %d times, want exactly once", timesReadCloserCalled)
				}

				if timesInnerCloserCalled != 1 {
					t.Errorf("Closer.Close() was called %d times, want exactly once", timesInnerCloserCalled)
				}
			})
		}
	})
}

func TestAppendCloser(t *testing.T) {
	tests := []struct {
		name       string
		readCloser io.ReadCloser
		closer     io.Closer
	}{
		{
			name:       "GoodReadCloserAndGoodCloser",
			readCloser: io.NopCloser(&bytes.Buffer{}),
			closer:     io.NopCloser(&bytes.Buffer{}),
		},
		{
			name: "BadReadCloserAndGoodCloser",
			readCloser: struct {
				io.Reader
				io.Closer
			}{
				iotest.ErrReader(errors.New("bad reader")),
				closerFunc(func() error { return errors.New("bad closer") }),
			},
			closer: io.NopCloser(&bytes.Buffer{}),
		},
		{
			name:       "GoodReadCloserAndBadCloser",
			readCloser: io.NopCloser(&bytes.Buffer{}),
			closer:     closerFunc(func() error { return errors.New("bad closer") }),
		},
		{
			name: "BadReadCloserAndBadCloser",
			readCloser: struct {
				io.Reader
				io.Closer
			}{
				iotest.ErrReader(errors.New("bad reader")),
				closerFunc(func() error { return errors.New("bad closer") }),
			},
			closer: closerFunc(func() error { return errors.New("bad closer") }),
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			wrapped := appendCloser(test.readCloser, test.closer)

			if wrapped, ok := wrapped.(wrappedReadCloser); !ok {
				t.Error("resulting closer was not a wrappedReadCloser")
			} else {
				if have, want := wrapped.ReadCloser, test.readCloser; have == nil && want != nil {
					t.Errorf("have nil, want %#v", want)
				} else if have != nil && want == nil {
					t.Errorf("have %#v, want nil", have)
					//} else if have != want {
					//	t.Errorf("have %#v, want %#v", have, want)
				}

				if have, want := wrapped.Closer, test.closer; have == nil && want != nil {
					t.Errorf("have nil, want %#v", want)
				} else if have != nil && want == nil {
					t.Errorf("have %#v, want nil", have)
					//} else if have != want {
					//	t.Errorf("have %#v, want %#v", have, want)
				}
			}

			t.Run("Read()", func(t *testing.T) {
				t.Skip()

				// wrapped.Read()
			})

			t.Run("Close", func(t *testing.T) {
				t.Skip()

				// wrapped.Close()
			})
		})
	}
}

func TestAppendCloserFunc(t *testing.T) {
	tests := []struct {
		name       string
		readCloser io.ReadCloser
		closer     closerFunc
	}{
		{
			name:       "GoodReadCloserAndGoodCloser",
			readCloser: io.NopCloser(&bytes.Buffer{}),
			closer:     func() error { return nil },
		},
		{
			name: "BadReadCloserAndGoodCloser",
			readCloser: struct {
				io.Reader
				io.Closer
			}{
				iotest.ErrReader(errors.New("bad reader")),
				closerFunc(func() error { return errors.New("bad closer") }),
			},
			closer: func() error { return nil },
		},
		{
			name:       "GoodReadCloserAndBadCloser",
			readCloser: io.NopCloser(&bytes.Buffer{}),
			closer:     closerFunc(func() error { return errors.New("bad closer") }),
		},
		{
			name: "BadReadCloserAndBadCloser",
			readCloser: struct {
				io.Reader
				io.Closer
			}{
				iotest.ErrReader(errors.New("bad reader")),
				closerFunc(func() error { return errors.New("bad closer") }),
			},
			closer: closerFunc(func() error { return errors.New("bad closer") }),
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			wrapped := appendCloserFunc(test.readCloser, test.closer)

			if wrapped, ok := wrapped.(wrappedReadCloser); !ok {
				t.Error("resulting closer was not a wrappedReadCloser")
			} else {
				if have, want := wrapped.ReadCloser, test.readCloser; have == nil && want != nil {
					t.Errorf("have nil, want %#v", want)
				} else if have != nil && want == nil {
					t.Errorf("have %#v, want nil", have)
					//} else if have != want {
					//	t.Errorf("have %#v, want %#v", have, want)
				}

				if have, want := wrapped.Closer, test.closer; have == nil && want != nil {
					t.Errorf("have nil, want %#v", want)
				} else if have != nil && want == nil {
					t.Errorf("have %#v, want nil", have)
					//} else if have != want {
					//	t.Errorf("have %#v, want %#v", have, want)
				}
			}

			t.Run("Read()", func(t *testing.T) {
				t.Skip()

				// wrapped.Read()
			})

			t.Run("Close", func(t *testing.T) {
				t.Skip()

				// wrapped.Close()
			})
		})
	}
}

func TestAppendCloserCancelFunc(t *testing.T) {
	t.Run("Read()", func(t *testing.T) {
		tests := []struct {
			name string
			want []byte
		}{
			{
				name: "Empty",
				want: []byte{},
			},
			{
				name: "Short",
				want: []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
			},
			{
				name: "Long",
				want: []byte(`# this is a long form twtxt file
# it contains some metadata, some comments, and some tweets
# note that none of the this is actually parsed or anything, it's just for
# testing various lengths of byte slices to the reader
# test only = true
2022-04-15T12:25:42+13:00	Ohh look a tweet.
2022-04-15T12:26:59+13:00	An another one, just ignore this for now.
`),
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				wrapped := appendCloserCancelFunc(
					io.NopCloser(bytes.NewReader(test.want)),
					nil,
				)

				if err := iotest.TestReader(wrapped, test.want); err != nil {
					t.Error(err)
				}
			})
		}
	})

	t.Run("Close", func(t *testing.T) {
		tests := []struct {
			name string
			want error
		}{
			{
				name: "ReaderCloseSucceeds",
				want: nil,
			},
			{
				name: "ReaderCloseFails",
				want: errors.New("expected error"),
			},
		}

		for _, test := range tests {
			test := test

			t.Run(test.name, func(t *testing.T) {
				var times int

				wrapped := appendCloserCancelFunc(
					struct {
						io.Reader
						io.Closer
					}{
						&bytes.Buffer{},
						closerFunc(func() error { return test.want }),
					},
					func() { times++ },
				)

				if have, want := wrapped.Close(), test.want; have != want {
					t.Errorf("have %q, want %q", have, want)
				}

				if times != 1 {
					t.Errorf("Close() was called %d times, want exactly once", times)
				}
			})
		}
	})
}

func TestBuildMetadataBlock(t *testing.T) {
	tests := []struct {
		name   string
		title  string
		fields []string
		values map[string][]string
		want   string
	}{
		{
			name:   "Empty",
			title:  "",
			fields: []string{},
			values: map[string][]string{},
			want:   ``,
		},
		{
			name:   "EmptyWithTitle",
			title:  "this is a comment block",
			fields: []string{},
			values: map[string][]string{},
			want:   ``,
		},
		{
			name:   "NoFields",
			title:  "this is a also comment block",
			fields: []string{},
			values: map[string][]string{
				"field a": []string{
					"value 1",
					"value 2",
					"value 3",
				},
				"field b": []string{
					"value 4",
				},
				"field c": []string{
					"",
				},
				"field d": []string{},
				"field e": nil,
			},
			want: ``,
		},
		{
			name:  "NoValues",
			title: "lorem ipsum",
			fields: []string{
				"field a",
				"field b",
				"field c",
				"field d",
				"field e",
			},
			values: map[string][]string{},
			want:   ``,
		},
		{
			name:  "FullBlock",
			title: "this is a comment block that actually is a block",
			fields: []string{
				"field a",
				"field g",
				"field b",
				"field m",
				"field c",
				"field e",
				"field j",
			},
			values: map[string][]string{
				"field a": []string{
					"value 1",
					"value 2",
					"value 3",
				},
				"field b": []string{
					"value 4",
				},
				"field c": []string{
					"",
				},
				"field d": []string{},
				"field e": nil,
				"field f": []string{
					"value 5",
					"value 6",
					"value 7",
				},
				"field g": []string{
					"value 8",
				},
				"field h": []string{
					"",
				},
				"field i": []string{},
				"field j": nil,
			},
			want: `# this is a comment block that actually is a block:
# field a = value 1
# field a = value 2
# field a = value 3
# field g = value 8
# field b = value 4
#`,
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			if diff := cmp.Diff(test.want, buildMetadataBlock(test.title, test.fields, test.values)); diff != "" {
				t.Error(diff)
			}
		})
	}
}
