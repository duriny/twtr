package twtr

// Source of a tweet, where the tweet was posted and who posted it.
type Source struct {
	Nick string
	URI  string
}

// String represents the source as a mention string.
//
// The mention syntax is pretty straightforward, if the source is missing a URI
// value, then the mention string is empty, as it is not possible to mention
// somebody by nickname alone, the URI is required.
//
//     @<URI>      // e.g. @<https://alice.example.com/twtxt.txt>
//     @<NICK URI> // e.g. @<bob https://bob.example.com/twtxt.txt>
//
// See https://twtxt.readthedocs.io/en/latest/user/twtxtfile.html
func (source Source) String() string {
	if source.URI == "" {
		return ""
	}

	if source.Nick == "" {
		return "@<" + source.URI + ">"
	}

	return "@<" + source.Nick + " " + source.URI + ">"
}
