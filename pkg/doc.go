// Package twtr is the backend to the twtr twtxt client.
//
//     go get duriny.envs.sh/twtr/pkg
//
// Note that this package imports under the name 'twtr' by default.
//
//     import twtr "duriny.envs.sh/twtr/pkg"
//
// This serves as an API and backend for the command line client, but also
// allows custom frontends to be built around it. See the cmd package for the
// reference frontend.
package twtr
