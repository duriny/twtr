package parse_test

import (
	"errors"
	"testing"

	. "duriny.envs.sh/twtr/pkg/parse"
	"github.com/google/go-cmp/cmp"
)

func TestCommentLine(t *testing.T) {
	cmpLine := cmp.AllowUnexported(
		Line{},
		CommentLine{},
		TweetLine{},
	)

	cmpErr := cmp.Comparer(func(left, right error) bool {
		if left == nil && right != nil {
			return false
		}

		if left != nil && right == nil {
			return false
		}

		if left == nil && right == nil {
			return true
		}

		if left == right {
			return true
		}

		if errors.Is(left, right) {
			return true
		}

		if errors.Is(right, left) {
			return true
		}

		if left, right := left.Error(), right.Error(); left == right {
			return true
		}

		return false
	})

	tests := []struct {
		name string
		from string
		want struct {
			text, field, value string
			err                error
		}
	}{
		{
			name: "EmptyLine",
			want: struct {
				text, field, value string
				err                error
			}{
				err: errors.New("comment line must start with '#'"),
			},
		},
		{
			name: "EmptyComment",
			from: "#",
			want: struct {
				text, field, value string
				err                error
			}{
				text: "#",
			},
		},
		{
			name: "BadComment",
			from: "this is not a comment",
			want: struct {
				text, field, value string
				err                error
			}{
				text: "this is not a comment",
				err:  errors.New("comment line must start with '#'"),
			},
		},
		{
			name: "NoMetadata",
			from: "# this is a comment",
			want: struct {
				text, field, value string
				err                error
			}{
				text: "# this is a comment",
			},
		},
		{
			name: "Metadata",
			from: "# foo = bar",
			want: struct {
				text, field, value string
				err                error
			}{
				text:  "# foo = bar",
				field: "foo",
				value: "bar",
			},
		},
		{
			name: "MetadataWithoutWhitespace",
			from: "#foo=bar",
			want: struct {
				text, field, value string
				err                error
			}{
				text:  "#foo=bar",
				field: "foo",
				value: "bar",
			},
		},
		{
			name: "MetadataWithExtraWhitespace",
			from: "#  foo \t\t = \t \t  bar",
			want: struct {
				text, field, value string
				err                error
			}{
				text:  "#  foo \t\t = \t \t  bar",
				field: "foo",
				value: "bar",
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			line := NewCommentLine(test.from)

			t.Run("Constructor", func(t *testing.T) {
				if diff := cmp.Diff(line.String(), test.want.text); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.FIELD, test.want.field); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.VALUE, test.want.value); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.Err(), test.want.err, cmpErr); diff != "" {
					t.Error(diff)
				}
			})

			t.Run("AsCommentLine", func(t *testing.T) {
				if comment, ok := line.AsCommentLine(); !ok {
					t.Error("conversion not ok")
				} else if diff := cmp.Diff(comment, line, cmpLine, cmpErr); diff != "" {
					t.Error(diff)
				}
			})

			t.Run("AsTweetLine", func(t *testing.T) {
				if tweet, ok := line.AsTweetLine(); ok {
					t.Errorf("conversion should not have succeeded: %#v", tweet)
				}
			})
		})
	}
}
