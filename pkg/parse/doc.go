// Package parse provides lexical analysis and parsing methods for twtr.
//
// While usable as a stand alone parsing package, the twtr.Client type is better
// suited to high level applications.
package parse
