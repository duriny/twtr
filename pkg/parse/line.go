package parse

// Line is the basic unit of a parsed line, it represents the original text of
// the line as it was parsed, as well as any error that occurred during parsing.
type Line struct {
	text string // the original text of the line
	err  error  // any error that might have occurred when the line was parsed
}

// String represents the line as it was originally parsed.
func (line Line) String() string {
	return line.text
}

// Err reports any error that may have occurred during parsing.
func (line Line) Err() error {
	return line.err
}
