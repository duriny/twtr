package parse

// FeedLine represents either a CommentLine or a TweetLine, as either may appear
// in a twtxt.txt feed file.
type FeedLine interface {
	// AsCommentLine reports whether this FeedLine is a CommentLine and casts it
	// appropriately
	AsCommentLine() (line CommentLine, ok bool)

	// AsTweetLine reports whether this FeedLine is a TweetLine and casts it
	// appropriately
	AsTweetLine() (line TweetLine, ok bool)

	// private ensures nobody else implements FeedLine
	private()
}
