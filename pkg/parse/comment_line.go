package parse

import (
	"errors"
	"strings"
)

// CommentLine represents a line, parsed as a comment in a twtxt.txt file.
type CommentLine struct {
	// These are the metadata fields of the line, parsed as:
	//
	//     FIELD = VALUE
	//
	// Note that these are not required and may be empty.
	FIELD, VALUE string

	// CommentLine extends the Line type
	Line
}

// NewCommentLine parses a new comment line from the given string.
func NewCommentLine(line string) (comment CommentLine) {
	comment = CommentLine{
		Line: Line{
			text: line,
		},
	}

	if !strings.HasPrefix(line, "#") {
		comment.err = errors.New("comment line must start with '#'")
	} else if strings.Contains(line, "=") {
		metadata := strings.SplitN(strings.TrimLeft(line, "#"), "=", 2)

		comment.FIELD = strings.TrimSpace(metadata[0])

		if len(metadata) == 2 {
			comment.VALUE = strings.TrimSpace(metadata[1])
		}
	}

	return
}

// AsCommentLine reports whether this FeedLine is a CommentLine and casts it
// appropriately.
func (comment CommentLine) AsCommentLine() (line CommentLine, ok bool) {
	comment.private()

	line, ok = comment, true

	return
}

// AsTweetLine reports whether this FeedLine is a TweetLine and casts it
// appropriately.
func (comment CommentLine) AsTweetLine() (line TweetLine, ok bool) {
	comment.private()

	return
}

// private marks a private implementer of FeedLine.
func (CommentLine) private() {}
