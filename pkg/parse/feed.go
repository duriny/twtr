package parse

import (
	"bufio"
	"io"
	"strings"
	"sync"
)

// Feed parses the data as a twtxt feed file.
//
// See https://twtxt.readthedocs.io/en/latest/user/twtxtfile.html#format-specification
func Feed(data io.Reader) <-chan FeedLine {
	lines := make(chan FeedLine)
	group := &sync.WaitGroup{}

	if data != nil {
		scanner := bufio.NewScanner(data)

		// scan the data into feed lines concurrently
		for scanner.Scan() {
			group.Add(1)
			go func(line string) {
				defer group.Done()

				if strings.HasPrefix(line, "#") {
					lines <- NewCommentLine(line)
				} else {
					lines <- NewTweetLine(line)
				}
			}(scanner.Text())
		}

		// pass error as feed line
		if err := scanner.Err(); err != nil {
			group.Add(1)
			go func(err error) {
				defer group.Done()

				lines <- TweetLine{
					Line: Line{
						err: err,
					},
				}
			}(err)
		}
	}

	// close the lines when the group is done
	go func() {
		group.Wait()

		close(lines)
	}()

	return lines
}
