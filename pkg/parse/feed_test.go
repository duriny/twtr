package parse_test

import (
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
	"testing"
	"testing/iotest"

	. "duriny.envs.sh/twtr/pkg/parse"
	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
)

func TestFeed(t *testing.T) {
	tests := []struct {
		name string
		from io.Reader
		want []FeedLine
	}{
		{
			name: "NilReader",
			from: nil,
			want: []FeedLine{},
		},
		{
			name: "EmptyReader",
			from: strings.NewReader(""),
			want: []FeedLine{},
		},
		{
			name: "BadReader",
			from: iotest.ErrReader(errors.New("expected error")),
			want: []FeedLine{
				TweetLine{},
			},
		},
		{
			name: "FullFile",
			from: strings.NewReader(strings.Join([]string{
				"# twtxt is decentralised microblogging for hackers",
				"#",
				"# Learn more about twtxt at https://twtxt.readthedocs.io",
				"#",
				"# About me:",
				"# nick        = ~duriny",
				"# url         = https://envs.net/~duriny/twtxt.txt",
				"# description = Author of twtr",
				"#",
				"# Following:",
				"# follow = computerphile https://feeds.twtxt.net/computerphile/twtxt.txt",
				"# follow = nzherald-co-nz-world https://feeds.twtxt.net/nzherald-co-nz-world/twtxt.txt",
				"# follow = twtr_news https://duriny.envs.sh/twtr/news.txt",
				"# follow = twtxt_news https://buckket.org/twtxt_news.txt",
				"# follow = xkcd https://feeds.twtxt.net/xkcd/twtxt.txt",
				"#",
				"2022-03-29T10:42:26+13:00\tHello World!",
				"2022-03-31T15:43:14+13:00\ttwtxt is decentralised microblogging for hackers",
			}, "\n")),
			want: []FeedLine{
				NewCommentLine("# twtxt is decentralised microblogging for hackers"),
				NewCommentLine("#"),
				NewCommentLine("# Learn more about twtxt at https://twtxt.readthedocs.io"),
				NewCommentLine("#"),
				NewCommentLine("# About me:"),
				NewCommentLine("# nick        = ~duriny"),
				NewCommentLine("# url         = https://envs.net/~duriny/twtxt.txt"),
				NewCommentLine("# description = Author of twtr"),
				NewCommentLine("#"),
				NewCommentLine("# Following:"),
				NewCommentLine("# follow = computerphile https://feeds.twtxt.net/computerphile/twtxt.txt"),
				NewCommentLine("# follow = nzherald-co-nz-world https://feeds.twtxt.net/nzherald-co-nz-world/twtxt.txt"),
				NewCommentLine("# follow = twtr_news https://duriny.envs.sh/twtr/news.txt"),
				NewCommentLine("# follow = twtxt_news https://buckket.org/twtxt_news.txt"),
				NewCommentLine("# follow = xkcd https://feeds.twtxt.net/xkcd/twtxt.txt"),
				NewCommentLine("#"),
				NewTweetLine("2022-03-29T10:42:26+13:00\tHello World!"),
				NewTweetLine("2022-03-31T15:43:14+13:00\ttwtxt is decentralised microblogging for hackers"),
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			lines := []FeedLine{}

			for line := range Feed(test.from) {
				lines = append(lines, line)
			}

			less := func(left, right FeedLine) bool {
				var ok bool
				var l, r fmt.Stringer

				if l, ok = left.AsCommentLine(); !ok {
					l, _ = left.AsTweetLine()
				}

				if r, ok = right.AsCommentLine(); !ok {
					r, _ = right.AsTweetLine()
				}

				return l.String() < r.String()
			}

			sort.SliceStable(lines, func(i, j int) bool { return less(lines[i], lines[j]) })
			sort.SliceStable(test.want, func(i, j int) bool { return less(test.want[i], test.want[j]) })

			if diff := cmp.Diff(
				test.want,
				lines,
				cmpopts.IgnoreUnexported(
					CommentLine{},
					TweetLine{},
					Line{},
				),
			); diff != "" {
				t.Errorf(diff)
			}
		})
	}
}
