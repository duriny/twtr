package parse_test

import (
	"errors"
	"fmt"
	"testing"
	"time"

	. "duriny.envs.sh/twtr/pkg/parse"
	"github.com/google/go-cmp/cmp"
)

func TestTweetLine(t *testing.T) {
	cmpLine := cmp.AllowUnexported(
		Line{},
		CommentLine{},
		TweetLine{},
	)

	cmpErr := cmp.Comparer(func(left, right error) bool {
		if left == nil && right != nil {
			return false
		}

		if left != nil && right == nil {
			return false
		}

		if left == nil && right == nil {
			return true
		}

		if left == right {
			return true
		}

		if errors.Is(left, right) {
			return true
		}

		if errors.Is(right, left) {
			return true
		}

		if left, right := left.Error(), right.Error(); left == right {
			return true
		}

		return false
	})

	tests := []struct {
		name string
		from string
		want struct {
			text, post string
			time       time.Time
			err        error
		}
	}{
		{
			name: "EmptyLine",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				err: errors.New("tweet line must have a tab delimiter"),
			},
		},
		{
			name: "EmptyTimestamp",
			from: "\tthis tweet is missing a timestamp",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "\tthis tweet is missing a timestamp",
				err:  errors.New("tweet line must have a timestamp"),
			},
		},
		{
			name: "BadTimestamp",
			from: "This is not a timestamp",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "This is not a timestamp",
				err:  errors.New("tweet line must have a tab delimiter"),
			},
		},
		{
			name: "BadTimestampWithTabDelimiter",
			from: "This is not a timestamp\t",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "This is not a timestamp\t",
				err: fmt.Errorf("tweet line must have a timestamp: %w", func() error {
					_, err := time.Parse(time.RFC3339, "This is not a timestamp")
					return err
				}()),
			},
		},
		{
			name: "VaildTimestampWithoutTabDelimiter",
			from: "2022-04-11T14:10:23+13:00",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "2022-04-11T14:10:23+13:00",
				err:  errors.New("tweet line must have a tab delimiter"),
			},
		},
		{
			name: "VaildTimestampWithEmptyMessage",
			from: "2022-04-11T14:10:23+13:00\t",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "2022-04-11T14:10:23+13:00\t",
				time: time.Date(2022, 04, 11, 14, 10, 23, 00, time.FixedZone("UTC+13", +13*60*60)),
			},
		},
		{
			name: "VaildTimestampWithMessage",
			from: "2022-04-11T14:10:23+13:00\tThis is a valid tweet",
			want: struct {
				text, post string
				time       time.Time
				err        error
			}{
				text: "2022-04-11T14:10:23+13:00\tThis is a valid tweet",
				time: time.Date(2022, 04, 11, 14, 10, 23, 00, time.FixedZone("UTC+13", +13*60*60)),
				post: "This is a valid tweet",
			},
		},
	}

	for _, test := range tests {
		test := test

		t.Run(test.name, func(t *testing.T) {
			line := NewTweetLine(test.from)

			t.Run("Constructor", func(t *testing.T) {
				if diff := cmp.Diff(line.String(), test.want.text); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.TIMESTAMP, test.want.time); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.MESSAGE, test.want.post); diff != "" {
					t.Error(diff)
				}

				if diff := cmp.Diff(line.Err(), test.want.err, cmpErr); diff != "" {
					t.Error(diff)
				}
			})

			t.Run("AsCommentLine", func(t *testing.T) {
				if comment, ok := line.AsCommentLine(); ok {
					t.Errorf("conversion should not have succeeded: %#v", comment)
				}
			})

			t.Run("AsTweetLine", func(t *testing.T) {
				if tweet, ok := line.AsTweetLine(); !ok {
					t.Error("conversion not ok")
				} else if diff := cmp.Diff(tweet, line, cmpLine, cmpErr); diff != "" {
					t.Error(diff)
				}
			})
		})
	}
}
