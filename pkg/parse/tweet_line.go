package parse

import (
	"errors"
	"fmt"
	"strings"
	"time"
)

// TweetLine represents a line, parsed as an entry in a twtxt.txt file.
type TweetLine struct {
	// These are the fields of the line, parsed as:
	//
	//    TIMESTAMP\tMESSAGE
	//
	// Note that any of these may be blank or missing, check the Err() method to
	// validate the line.
	TIMESTAMP time.Time
	MESSAGE   string

	// TweetLine extends the Line type
	Line
}

// NewTweetLine parses a new tweet line from the given string.
func NewTweetLine(line string) (tweet TweetLine) {
	tweet = TweetLine{
		Line: Line{
			text: line,
		},
	}

	if !strings.Contains(line, "\t") {
		tweet.err = errors.New("tweet line must have a tab delimiter")
	} else if parts := strings.SplitN(line, "\t", 2); len(parts) < 1 || parts[0] == "" {
		tweet.err = errors.New("tweet line must have a timestamp")
	} else if timestamp, err := time.Parse(time.RFC3339, parts[0]); err != nil {
		tweet.err = fmt.Errorf("tweet line must have a timestamp: %w", err)
	} else {
		tweet.TIMESTAMP = timestamp
		tweet.MESSAGE = strings.Join(parts[1:], "")
	}

	return
}

// AsCommentLine reports whether this FeedLine is a CommentLine and casts it
// appropriately.
func (tweet TweetLine) AsCommentLine() (line CommentLine, ok bool) {
	tweet.private()

	return
}

// AsTweetLine reports whether this FeedLine is a TweetLine and casts it
// appropriately.
func (tweet TweetLine) AsTweetLine() (line TweetLine, ok bool) {
	tweet.private()

	line, ok = tweet, true

	return
}

// private marks a private implementer of FeedLine.
func (TweetLine) private() {}
