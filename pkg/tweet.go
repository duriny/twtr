package twtr

import (
	"fmt"
	"io"
	"strings"
	"time"
)

// multilineExtension implements the multi-line tweet extension.
//
// Literal new lines are replaced with the U+2028 Line Separator character, in
// accordance with https://dev.twtxt.net/multilineextension.html
var multilineExtension = strings.NewReplacer("\n", "\u2028").Replace

// Tweet represents a posted message, when it was posted, and where it was
// posted (if the source is known).
type Tweet struct {
	// Source of the tweet.
	Source

	// Time that the tweet was posted.
	Time time.Time

	// Text of the tweet.
	Text string
}

// NewTweet creates a new tweet from the given message with the time set to
// time.Now().
func NewTweet(message string) Tweet {
	return Tweet{
		Time: time.Now(),
		Text: message,
	}
}

// String formats the tweet as a string representing the tweet according to the
// twtxt.txt file specification.
func (tweet Tweet) String() string {
	time := tweet.Time.Format(time.RFC3339)
	text := multilineExtension(strings.TrimSpace(tweet.Text))

	return time + "\t" + text
}

// WriteTo writes the tweet to the given writer according to the twtxt.txt file
// specification.
//
//     TIMESTAMP\tMESSAGE\n
//
// See also: https://twtxt.readthedocs.io/en/latest/user/twtxtfile.html
func (tweet Tweet) WriteTo(w io.Writer) (n int64, err error) {
	count, err := fmt.Fprintln(w, tweet)

	return int64(count), err
}
