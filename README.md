# twtr

[![Go Reference](https://pkg.go.dev/badge/duriny.envs.sh/twtr.svg)](https://pkg.go.dev/duriny.envs.sh/twtr)
[![Go Report Card](https://goreportcard.com/badge/duriny.envs.sh/twtr)](https://goreportcard.com/report/duriny.envs.sh/twtr)

*A modular, hackable, and decentralised, microblogging client for the twtxt
protocol.*

# DEPRECATED

> The twtxt (and more recently Yarn.Social) ecosystem is a great alternative to
> modern social media. It was very rewarding to contribute to that ecosystem
> both in terms of interacting with other users, and creating tools to make that
> ecosystem better. However, due to a recent injury and the subsequent time I
> spent offline, I realised how much my mental health improved away from my
> phone.
>
> Working on `twtr` was always driven by my own usage, and since that has
> declined I don't have as much to add. I dedicate the work I have done this far
> to the public domain. If you want to use `twtr` or take it on and modify it
> for your own purposes, please do so.
>
> ~duriny

## Installation

```shell
go install duriny.envs.sh/twtr@latest
```

## Usage

The general syntax is `twtr COMMAND [OPTIONS] [ARGS ...]`, if you get stuck on
any command, don't be afraid to add `--help` to the command.

### Configuration

All configuration is handled via the metadata fields in your twtxt.txt file. The
most important fields are `nick`, `url`, and the `follow` directives. Use the
`twtr init` command to get started, after that, you can manually edit the
`~/.local/share/twtr/twtxt.txt` file to add or change the metadata.

See the metadata [spec](https://dev.twtxt.net/doc/metadataextension.html) for
more details.

### Hooks

`twtr` uses `git` style hooks, it looks for special files in the hook directory,
`~/.config/twtr/hooks/`, and if the hook file exists and is executable, it is
gets run at the appropriate event.

* `pre-tweet` is executed directly *before* a tweet is posted.
* `post-tweet` is executed directly *after* a tweet is posted.

See the [`twtr.Hook`](https://pkg.go.dev/duriny.envs.sh/twtr/pkg#Hook) type for
more examples.

## FAQ

### What is twtxt?

[Twtxt](https://twtxt.readthedocs.io) is a self-hosted microblogging platform,
think Twitter, but way simpler.

Instead of a centralized server with a company behind it, each twtxt user hosts
their "feed" as a plain text file, `twtxt.txt`, on their own website. Other
users can then read that feed and make posts of their own, even replying or
tagging each other.

You can read more about the twtxt file format
[here](https://twtxt.readthedocs.io/en/latest/user/twtxtfile.html).

### Ok, So what is `twtr` then?

`twtr` is a client for twtxt, it helps you write tweets (or twts) to your
twtxt.txt file, and makes it easier to follow other feeds and get updates from
the people you follow.

There are a lot of twtxt clients out there, you can check those out as well, or
you can try writing your own:

* [jenny](https://www.uninformativ.de/git/jenny/file/README.html) - A client
  that integrates with [mutt](http://www.mutt.org/).
* [twet](https://github.com/jdtron/twet) - A command line client.
* [Yarn.Social](https://yarn.social/) - A web interface and twtxt hosting
  platform.

### Can I hack on `twtr`?

Of course! All patches, pull requests, suggestions, and/or questions are
welcome. `twtr` even encourages forking, the command line interface is modular,
so you can easily clone this repo, or write your own `main.go`. The more people
use twtxt, the more the community grows.

### How do I pronounce `twtr`?

Like "tweeter", i.e. I am the one who tweets.

### Where do I start?

Just run `twtr init`.

### What If I don't have a website?

That's ok, you can host your twtxt.txt file anywhere on the internet. There are
a couple of good places to consider:

* You can join a tilde on the [tildeverse](https://tildeverse.org), then you can
  just host your feed at `https://example.com/~username/twtxt.txt`.
* If you don't want to create a whole website, you can join a Yarn.Social pod,
  such as [https://twtxt.net](https://twtxt.net). Yarn.Social is an excellent
  choice for beginners.
* *Coming Soon* - Twtxt feeds may be hosted as a Gist on https://github.com.

### How does `twtr` send my tweets to the hosting server?

It depends on where you host your twtxt.txt file, if you have ssh access to the
server you host it on, you can use the `scp(1)` command in the
`~/.config/twtr/hooks/pre-tweet` file to copy the remote file to your local
machine, and use `scp(1)` again to copy the local file back to the server after
the tweet has been written to it using `~/.config/twtr/hooks/post-tweet`.

